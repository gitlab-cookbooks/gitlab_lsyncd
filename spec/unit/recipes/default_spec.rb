# frozen_string_literal: true

# Cookbook:: gitlab_lsyncd
# Spec:: default
#
# Copyright:: 2018, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_lsyncd::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04')
                            .converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'has correct default attributes' do
      expect(chef_run.node['gitlab_lsyncd']['pages']['target_host']).to eq('127.0.0.1') # rubocop:disable Metrics/LineLength
    end
  end
end
