# frozen_string_literal: true

default['gitlab_lsyncd']['pages']['src_dir'] = '/var/opt/gitlab/gitlab-rails/shared/pages' # rubocop:disable Metrics/LineLength
default['gitlab_lsyncd']['pages']['target_dir'] = '/var/opt/gitlab/gitlab-rails/shared/pages' # rubocop:disable Metrics/LineLength
default['gitlab_lsyncd']['pages']['target_host'] = '127.0.0.1'
