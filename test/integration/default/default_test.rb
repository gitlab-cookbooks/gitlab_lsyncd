# frozen_string_literal: true

# InSpec tests for recipe template::default

control 'general-checks' do
  impact 1.0
  title 'General tests for template cookbook'
  desc '
    This control ensures that:
      * there is no duplicates in /etc/group'

  describe etc_group do
    its('gids') { should_not contain_duplicates }
  end
end
