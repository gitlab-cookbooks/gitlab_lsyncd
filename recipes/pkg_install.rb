# frozen_string_literal: true

# Cookbook Name:: gitlab_lsyncd
# Recipe:: pkg_install
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

package 'lsyncd' do
  action :upgrade
end

# Where configuration files go
directory '/etc/lsyncd' do
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end

# Where log files go
directory '/var/log/lsyncd' do
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end
