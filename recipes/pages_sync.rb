# frozen_string_literal: true

# Cookbook Name:: gitlab_lsyncd
# Recipe:: pages_sync
# License:: MIT
#
# Copyright 2018, GitLab Inc.

template '/etc/lsyncd/pages-sync.lua' do
  source 'pages-sync.cfg.erb'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end
